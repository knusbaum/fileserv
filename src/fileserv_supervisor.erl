-module(fileserv_supervisor).
-behavior(supervisor).
-export([init/1]).

init([]) ->
    {ok, {{one_for_one, 5, 10},
          [{fileserv_serv,
            {fileserv_serv, start_link, []},
            permanent,
            10000,
            worker,
            [fileserv_serv]}]}}.
